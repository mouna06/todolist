<?php

class Item{
    
  public $nom;
  public $content;
   public $date;
   private $todolist;

  function __construct($nom,$content,$date)
  {
      $this->nom = $nom ;
      $this->content = $content;
      $this->date = $date; 
  }



  public function isValidContent(){
    if (strlen($this->content) >= 1000){
        throw new \Exception('Your item is content is too long');
    }
    return true;
}
private function isNotEmpty($var, $type)
{
    if (!strlen($var) > 0) {
        throw new \Exception($type. ' should be not empty');
    }
    return true;
}


    public function isValid(){
        if (
        $this->isNotEmpty($this->nom, 'nom') &&
        $this->isNotEmpty($this->date, 'date') &&
        $this->isValidContent()
    ){
        return true;
    }
    return false;
    }


}