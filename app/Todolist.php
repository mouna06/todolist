<?php
require_once './services/EmailService.php';



// service 
class Todolist{
    

    public $id;
    public $items = array(10);
    public $user;
  
    function __construct($id,$items,$user)
    {
        $this->id = $id ;
        $this->items = $items;
        $this->user = $user;
    }


    function intervallBetweenTwoDates($date1,$date2){
       $to_time = strtotime($date1);
       $from_time = strtotime($date2);
       return (round(abs($to_time - $from_time) / 60,2));

    }

// if it can add item it returns item,



    function validIntervall($item){
         

      foreach ($this->items as $value){

        if ($this->intervallBetweenTwoDates($value->date,$item->date) < 30){
                throw new \Exception('Invalid intervall between two items');
                return false;
            }

        }
        return true;
    }
     function addItem($item)
    {
        if (count($this->items) >= 10) {
            throw new \Exception('You should have maximum 10 items');
            return null;
        }

        if (!in_array($item,$this->items) && $item->isValid() && $this->user->isValid()) {


            $this->items[] = $item;
        }
        return $item;
        
    }

    public function getItems(){
        return $this->items;
    }




function canSendItem($item){

    $serviceMailer = new EmailService;
    $serviceMailer->send($item,$this->user);


    return $item;
}




}