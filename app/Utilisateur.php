<?php
class Utilisateur{
  public $id;
  public $nom;
  public $prenom;
  public $email;
  public $password;
  public $birthday;
  private $todolist = null;
  function __construct($id,$nom,$prenom,$email,$password,$birthday)
  {

      $this->id = $id ;
      $this->nom = $nom ;
      $this->prenom = $prenom;
      $this->email = $email;
      $this->password = $password;
      $this->birthday = $birthday; 
  }
 
  public function inDB()
  {
    $bdd = new PDO('mysql:host=localhost;dbname=todolist;charset=utf8', 'root', 'user');
    $reponse = $bdd->query('SELECT * FROM todolist WHERE id_user LIKE $this->$id');
    while ($donnees = $reponse->fetch()){
       echo $donnees['id'];
    }    

  }

  // one todolist per user


  function addTodolist ($todolist){
    if (!$this->todolist){
    $this->todolist = $todolist;
    }else {            
      throw new \Exception('You should have only one todolist');
    }
  }

  function getTodolist(){
    return $this->todolist;
  }



  function addItem($item){
    if ($this->todolist()){
      echo 'no todolist for this user';
    }else {
      $this->todolist()->addItem($item);
    }
  }

  public function isValidBirthday()
  {

      if (((new DateTime('now'))->diff(date_create($this->birthday))->y) < 13)
          throw new \Exception('Vous devez avoir plus de 13 ans');
  
      return true;
  }
  

  private function isNotEmpty($var, $type)
{
    if (!strlen($var) > 0) {
        throw new \Exception($type. ' should be not empty');
    }
    return true;
}

private function isPasswordValid(){
  if (strlen($this->password) < 8 || strlen($this->password) > 40){
      throw new \Exception('Your password must be beteen 8 and 40 characters long');
  }
  return true;
}
function isValideEmail(){    
  return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $this->email)) ? FALSE : TRUE;
} 

function isValidToSendEmail(){
  if (((new DateTime('now'))->diff(date_create($this->birthday))->y) < 18)
  throw new \Exception('Vous devez avoir plus de 18 ans pour envoyer un email');

return true;
}

  public function isValid(){
    if ($this->isValidBirthday() &&
        $this->isValideEmail() &&
        $this->isNotEmpty($this->nom, 'nom') &&
        $this->isNotEmpty($this->prenom, 'prenom') &&
        $this->isPasswordValid()
    ){
        return true;
    }
    return false;
}


}