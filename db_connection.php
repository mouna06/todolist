<?php

// this class is for connect & creating tables in db

$servername = "localhost";
$username = "root";
$password = "user";
$dbname = "todolist";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// sql to create table
$sql = "CREATE TABLE user (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30) NOT NULL,
email VARCHAR(50),
pass VARCHAR(50),
birth VARCHAR(50)
)";

$sql2 = "CREATE TABLE item (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(30) NOT NULL,
    content VARCHAR(30) NOT NULL,
    creation VARCHAR(50)
    )";



if (mysqli_query($conn, $sql) && mysqli_query($conn, $sql2)) {
    echo "Tables created successfully";
} else {
    echo "Error creating table: " . mysqli_error($conn);
}



mysqli_close($conn);
?>