<?php
require 'app/Utilisateur.php';
require 'app/Item.php';
require_once 'services/EmailService.php';

use PHPUnit\Framework\TestCase;

class TestMail extends TestCase
{


    public function testSend()
    {
        $item = new Item("Item1", "contenu item 1",date("Y-m-d H:i"));
        $serviceMailer = new EmailService;
        // Créer un bouchon pour la classe Utilisateur.
        $suser = $this->getMockBuilder(Utilisateur::class)
                     ->disableOriginalConstructor()
                     ->disableOriginalClone()
                     ->disableArgumentCloning()
                     ->disallowMockingUnknownTypes()
                     ->getMock();

        // Configurer le bouchon.
        $suser->method('isValidToSendEmail')
             ->willReturn(true);

        // Appeler $suser->isValidToSendEmail() retournera désormais
        // true
        $this->assertSame(true, $suser->isValidToSendEmail());

        $this->assertTrue($serviceMailer->Send($item,$suser));
    }



}