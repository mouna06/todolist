<?php
require './app/Utilisateur.php';
require './app/Item.php';
require './app/Todolist.php';
require_once './services/EmailService.php';
use PHPUnit\Framework\TestCase;

class TestTodolist extends TestCase
{

     public function testcountItemsInTodolilst(){

        $stub = $this->createMock(Utilisateur::class);

        $todolist = new Todolist(2,[],$stub);
        for ($i=0; $i < 12; $i++) { 
            $item = new Item("Item".$i."", "contenu item'.$i.",date("Y-m-d H:i"));
            $stub->method('isValid')
            ->willReturn(true);
            $todolist->addItem($item);
            $this->expectExceptionMessage('You should have maximum 10 items');

        }
        $this->assertCount(10, $todolist->getItems());


    }  

    public function testvalidIntervall(){


        $stub = $this->createMock(Utilisateur::class);
        $todolist = new Todolist(2,[],$stub);

            $item1 = new Item("Item1", "contenu item.",date("2020-03-15 12:15"));
            $item2 = new Item("Item2", "contenu item.",date("2020-03-15 12:30"));
            $stub->method('isValid')
            ->willReturn(true);
            $todolist->addItem($item1);

            $this->expectExceptionMessage('Invalid intervall between two items');

        $todolist->validIntervall($item2);

    }  
    public function testaddItem(){
        $stub = $this->createMock(Utilisateur::class);
        $stub->method('isValid')
        ->willReturn(true);

        $todolist = new Todolist(4,[],$stub);
        $item = new Item ("nom","content",date("Y-m-d H:i"));
        
      $this->assertEquals($item,$todolist->addItem($item));

    } 

}