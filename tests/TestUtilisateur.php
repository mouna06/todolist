<?php
require_once './app/Utilisateur.php';
require './app/Item.php';
require './app/Todolist.php';

use PHPUnit\Framework\TestCase;

class TestUtilisateur extends TestCase
{
   
     public function testisValid()
    {
        $user = new Utilisateur(1,"nom","prenom","nom@gmail.com","password","12/01/1983");
        $this->assertTrue($user->isValid());

    }

    public function testisValidBirthday(){
        $user = new Utilisateur(2,"nom","prenom","nomgmailcom","password","12/01/2020");
        $this->expectExceptionMessage('Vous devez avoir plus de 13 ans');
        $user->isValidBirthday();

    }

    public function testisValidToSendEmail(){
        $user = new Utilisateur(2,"nom","prenom","nomgmailcom","password","12/01/2020");
        $this->expectExceptionMessage('Vous devez avoir plus de 18 ans pour envoyer un email');
        $user->isValidToSendEmail();
    }


      // test if user can have more than one todolist
      public function testaddTodolist(){
        
        $user = new Utilisateur(3,"nom","prenom","nom@gmail.com","password","12/01/1983");
        $todolist = new Todolist(1,[],$user);

        for ($i=1; $i<3; $i++) { 
        $todolist = new Todolist($i,[],$user);
        $user->addTodolist($todolist);
        $this->expectExceptionMessage('You should have only one todolist');

      }
      $this->assertEquals($todolist,$user->getTodolist());

    }


}